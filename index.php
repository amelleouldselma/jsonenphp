<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Computer science figures</h1>
  <div class="ui three stackable cards">
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use League\Csv\Reader;

require 'vendor/autoload.php';
$bio = Reader::createFromPath('cs_figures.csv', 'r');
$bio->setHeaderOffset(0); //set the CSV header offset



foreach ($bio as $data) {
    ?>

<div class="ui card">
  <div class="image">
    <img src="<?php echo $data['picture']; ?>">
  </div>
  <div class="content">
    <a class="header"><?php echo $data['name']; ?></a>

    <div class="extra content">
    <a>
      <i class="user icon"></i>
      <?php echo $data['title']; ?>
    </a>

  </div>
    <div class="description">
    <?php echo $data['role']; ?>
    </div>
  </div>

  

  <div class="meta">
      <span class="date">Born in <?php echo $data['birthyear']; ?></span>
    </div>
</div>
    

<?php

  }

?>






<script src="semantic/dist/semantic.min.js"></script>

</div>

</body>
</html>